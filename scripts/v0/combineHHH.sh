#!/bin/bash

# Define all relevant filepaths
Mod_Conf_Path=HHH_comb_2022/config/v0/modification
Comb_Conf_Path=HHH_comb_2022/config/v0/combination
Reparam_WS_Path=HHH_comb_2022/workspaces/v0/reparam_input
Out_Path=HHH_comb_2022/workspaces/v0/combination

# Make sure all directories that will be written to exist
mkdir -vp $Reparam_WS_Path
mkdir -vp $Out_Path
echo "Directories created"

echo "Starting reparameterization"
echo "Modifying singleH"
manager -w edit -x $Mod_Conf_Path/modify_singleH_STXS.xml

echo "Modifying HH bbyy"
#manager -w edit -x $Mod_Conf_Path/modify_bbyy.xml
manager -w edit -x $Mod_Conf_Path/modify_bbyy_pre.xml

echo "Modifying HH bbtautau"
manager -w edit -x $Mod_Conf_Path/modify_bbtautau.xml

echo "Modifying HH bbbb"
manager -w edit -x $Mod_Conf_Path/modify_bbbb.xml

echo "All workspaces reparametrized"


echo "Creating combination configuration from template and individual systematics files"

### HH comb
python HHH_comb_2022/macro/createXML.py $Comb_Conf_Path/template/combination_template_HH.xml $Comb_Conf_Path/combination_HH.xml
manager -w combine -x $Comb_Conf_Path/combination_HH.xml -f $Out_Path/WS-Comb-HH_kl.root -s 0 -t 1 | tee $Out_Path/WS-Comb-HH_kl.log

### H+HH comb
python HHH_comb_2022/macro/createXML.py $Comb_Conf_Path/template/combination_template_HHH.xml $Comb_Conf_Path/combination_HHH.xml
manager -w combine -x $Comb_Conf_Path/combination_HHH.xml -f $Out_Path/WS-Comb-HHH_kl.root -s 0 -t 1 | tee $Out_Path/WS-Comb-HHH_kl.log

echo "Finished"
