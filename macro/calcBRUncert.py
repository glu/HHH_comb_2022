#!/usr/bin/python

import sys, os, time, commands, getopt, copy, pickle, math, numpy
from glob import glob
from ROOT import *

# aS      mb      mc      TH bb   TH tautau   TH mumu   TH cc   TH gg   TH VV   TH yy   TH Zy
BRUncert={
    "bb" 	:[-0.78 ,0.71 ,-0.15 ,0.21 ,-0.03 ,0.00 ,-0.01 ,-0.26 ,-0.12 ,0.00 ,-0.01 ],
    "tautau" 	:[0.63 ,-0.99 ,-0.15 ,-0.29 ,0.47 ,0.00 ,-0.01 ,-0.26 ,-0.12 ,0.00 ,-0.01 ],
    "mumu" 	:[0.63 ,-0.99 ,-0.15 ,-0.29 ,-0.03 ,0.50 ,-0.01 ,-0.26 ,-0.12 ,0.00 ,-0.01],
    "cc" 	:[-0.38 ,-0.99 ,5.18 ,-0.29 ,-0.03 ,0.00 ,0.49 ,-0.26 ,-0.12 ,0.00 ,-0.01 ],
    "gg" 	:[3.65 ,-0.99 ,-0.15 ,-0.29 ,-0.03 ,0.00 ,-0.01 ,2.94 ,-0.12 ,0.00 ,-0.01 ],
    "gamgam" 	:[0.63 ,-0.99 ,-0.15 ,-0.29 ,-0.03 ,0.00 ,-0.01 ,-0.26 ,-0.12 ,1.00 ,-0.01],
    "Zgam" 	:[0.63 ,-0.99 ,-0.15 ,-0.29 ,-0.03 ,0.00 ,-0.01 ,-0.26 ,-0.12 ,0.00 ,4.99 ],
    "VV" 	:[0.63 ,-0.99 ,-0.15 ,-0.29 ,-0.03 ,0.00 ,-0.01 ,-0.26 ,0.38 ,0.00 ,-0.01 ]
}

if len(sys.argv)<3:
    print "Usage: python "+sys.argv[0]+" <channel 1> <channel 2>"
    print BRUncert
    sys.exit()
else:
    print sys.argv
    
ch1=sys.argv[1]
ch2=sys.argv[2]

uncertArr=[]
for idx in range(len(BRUncert[ch1])):
    if BRUncert[ch1][idx]*BRUncert[ch2][idx]>0: uncertArr.append(numpy.sign(BRUncert[ch1][idx])*(math.fabs(BRUncert[ch1][idx])-math.fabs(BRUncert[ch2][idx])))
    else: uncertArr.append(numpy.sign(BRUncert[ch1][idx])*(math.fabs(BRUncert[ch1][idx])+math.fabs(BRUncert[ch2][idx])))

print uncertArr

uncert=0
for number in uncertArr: uncert+=number*number

print math.sqrt(uncert)

