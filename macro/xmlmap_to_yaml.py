import glob, os, sys, argparse
import xml.etree.ElementTree as ET
import yaml
import pandas as pd

########################################################################################################################################################
# This script was writen to extract the naming map of nuisance parameters from xml files.                                                              #
# It was writen to work on v8/combination/combination_mu.xml and is not guaranteed to work on all xml files, please edit the xml function accordingly. #
# Dependancies are: Python3, modules: xml pandas PyYaml. Still need to find appropiate python setup on LXPLUS.                                         #
# Note that some OLDNAME do not have unique NEWNAME and might have NEWNAME1 and NEWNAME2 which depends on the channel.                                 #
# Therefore a single YAML is made per channel in the xml. The code will complain if NEWNAME is not unique.                                             #
########################################################################################################################################################


# def xml_to_dataframe(xmlfile="config/v8/combination/combination_mu.xml"):
#     '''
#     Functionality is there for dataframe, however it is not used at the moment to export to yaml
#     '''
#     #parse the xml
#     parsedXML = ET.parse( xmlfile )
#     # empty dataframe to hold NP information
#     dfcols = ['oldname','newname', 'type','category']
#     df_xml = pd.DataFrame(columns=dfcols)
#     # Find and loop through all channels in XML
#     chan = parsedXML.findall("Channel")
#     for ch in chan:
#         print ("Finding NP NameMap in channel {}".format(ch.attrib.get("Name")))
#         c = ch.find("RenameMap")
#         if c is None:
#             continue
#         sys = c.findall("Syst")
#         for s in sys:
#             df_xml = df_xml.append(pd.Series([s.get("OldName").split("(")[0],s.get("NewName"),s.get("NewName").split("_")[0],ch.attrib.get("Name")],index=dfcols),ignore_index=True)
#     return df_xml


def xml_to_dict(xmlfile="config/v8/combination/combination_mu.xml"):
    #parse the xml
    parsedXML = ET.parse( xmlfile )

    # empty dataframe to hold NP information
    data = {}
    
    # Find and loop through all channels in XML
    chan = parsedXML.findall("Channel")
    for ch in chan:
        channame = ch.attrib.get("Name")
        print ("Finding NP NameMap in channel {}".format(channame))
        c = ch.find("RenameMap")
        if c is None:
            continue
        data[channame] = {}
        sys = c.findall("Syst")
        for s in sys:
            oldname = s.get("OldName").split("(")[0]
            if oldname in data and data[channame][oldname] != s.get("NewName"):
                print ("{old} exists in dic -> {prev}, should it be {new}?".format(old=oldname,prev=data[channame][oldname],new=s.get("NewName")))
            data[channame][oldname]=s.get("NewName")
    return data


if __name__=="__main__" :
    
    parser=argparse.ArgumentParser()
    parser.add_argument("-x", "--xml", type=str, help="Input XML file path", required=True)
    parser.add_argument("--outpath", help="Output Yaml Path", type=str, default="./")

    args=parser.parse_args()


    d = xml_to_dict(args.xml)
    for chan in d:
        with open('{}/{}_map.yml'.format(args.outpath,chan), 'w') as outfile:
            yaml.dump(d[chan],outfile,default_flow_style=False)
