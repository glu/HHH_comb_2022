import argparse
from ROOT import *

parser=argparse.ArgumentParser()
parser.add_argument("-x", "--xml", help="Input XML file path", action="store")
parser.add_argument("-w", "--wsName", help="Workspace name", action="store")

args=parser.parse_args()
pOIFile=open(args.xml,"r")

wsFilePath=""
for iLine in pOIFile:
    if "InFile" not in iLine:
        continue

    wsFilePath=iLine.replace(" ","").split("\"")[1]
    print wsFilePath
    break

wsFile=TFile(wsFilePath)
workspace=wsFile.Get(args.wsName)

pOIFile.seek(0)
editTerm=pOIFile.read().replace(" ","").replace("\n","").replace("\t","").split("OLDPDF,")[1].split(")\"")[0]
editList=editTerm.split(",")
#print editList

pOIFile.seek(0)
modifyItem=pOIFile.read().replace(" ","").replace("\n","").replace("\t","")
modifyItemList=modifyItem.split("<ItemName=\"")

POISet=workspace.set("ModelConfig_POI")
iter=POISet.createIterator()
parg=iter.Next()

outFile=open("%s_POI.csv"%((args.xml).split("modify_")[1].split(".")[0]),"w")
outFile.write("Old,Mediate,New\n")

while parg:
    POIName=parg.GetName()
    print POIName

    if ("%s="%POIName) in editTerm:
        POIModified=editTerm.split("%s="%POIName)[1].split(",")[0]

        POIReplaced=""
        for iItem in range(len(modifyItemList)):
            if ("::%s("%POIModified) in modifyItemList[iItem]:
                POIReplaced=modifyItemList[iItem].split("\"/>")[0]
        #POIReplaced=modifyItem.split("::%s("%POIModified)[1].split(",")[0]
        #if ("prod::%s"%POIModified) in modifyItem:
        #    POIReplaced=modifyItem.split("::%s("%POIModified)[1].split(",")[0]
        #elif ("expr::%s"%POIModified) in modifyItem:
        #    POIReplaced=modifyItem.split("::%s("%POIModified)[1].split("\',")[1].split(",")[0]

        outFile.write("%s,%s,\"%s\"\n"%(POIName,POIModified,POIReplaced))
    else:
        outFile.write("%s\n"%POIName)

    parg=iter.Next()
