using namespace RooFit;
using namespace RooStats;

void scaleggFSTXS(TString inputFileName="HCombCouplingFall2017/workspaces/v9/combination/WS-Comb-xs_noBR_80ifb_splitggZH.root", TString outputFileName="HCombCouplingFall2017/workspaces/v9/combination/WS-Comb-stxs_splitggZH.root"){
  TFile *f=TFile::Open(inputFileName);
  RooWorkspace *w=(RooWorkspace*)f->Get("combWS");
  ModelConfig *mc=(ModelConfig*)w->obj("ModelConfig");
  RooArgSet *XS=(RooArgSet*)w->allVars().selectByName("XS_BR_ggF_*,norm_XS_ggH_channel_tautau");
  //RooArgSet *XS=(RooArgSet*)w->allVars().selectByName("XS13_gg2H_*");

  double scale=(4.852E+01+4.863E-01)/4.852E+01;
  TIterator *iter=XS->createIterator();
  RooRealVar *parg=NULL;
  while((parg=(RooRealVar*)iter->Next())){
    double currentVal=parg->getVal();
    parg->setRange(-10,10);
    parg->setVal(currentVal*scale);
    cout<<parg->GetName()<<" "<<currentVal<<" -> "<<parg->getVal()<<endl;
  }
  w->writeToFile(outputFileName);
}
