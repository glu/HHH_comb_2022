using namespace RooFit;
using namespace RooStats;
using namespace std;

void recoverSet(RooArgSet set, RooArgSet snapshot){
  TIterator *iter = set.createIterator();
  RooRealVar* parg = NULL, *parg_ss = NULL;
  while((parg=(RooRealVar*)iter->Next())){
    TString varName=parg->GetName();
    parg_ss=(RooRealVar*)snapshot.find(varName);
    if(parg_ss)parg->setVal(parg_ss->getVal());
  }
}


void addSnapshot(TString inputFileName, TString referenceFileName, TString outputFileName, bool importAsimov=true){

  TFile *file = TFile::Open(inputFileName,"READ");
  RooWorkspace* w = (RooWorkspace*) file->Get("combWS");

  ModelConfig* mc = (ModelConfig*) w->obj("ModelConfig");
  RooArgSet glob = *mc->GetGlobalObservables();
  RooArgSet nuis = *mc->GetNuisanceParameters();

  w->saveSnapshot("nominalGlobs", *mc->GetGlobalObservables());
  w->saveSnapshot("nominalNuis", *mc->GetNuisanceParameters());


  //save snapshot from the post-fit asimov dataset
  TFile *fRef=TFile::Open(referenceFileName);
  RooWorkspace *wRef=(RooWorkspace*)fRef->Get("combWS");
  ModelConfig *mcRef=(ModelConfig*)wRef->obj("ModelConfig");

  wRef->loadSnapshot("conditionalGlobs_1");
  RooArgSet* gobrefs = const_cast<RooArgSet*>(mcRef->GetGlobalObservables());
  RooArgSet* nuirefs = const_cast<RooArgSet*>(mcRef->GetNuisanceParameters());

  recoverSet(glob, *gobrefs);
  w->saveSnapshot("conditionalGlobs_1",glob);

  recoverSet(nuis, *nuirefs);
  w->saveSnapshot("conditionalNuis_1",nuis);



  // //put global observables back to normal by using the nominal combined workspace
  // TFile *fNom=TFile::Open(nominalFileName);
  // RooWorkspace *wNom=(RooWorkspace*)fNom->Get("combWS");
  // ModelConfig *mcNom=(ModelConfig*)wNom->obj("ModelConfig");
  // RooArgSet* gobNoms = const_cast<RooArgSet*>(mcNom->GetGlobalObservables());
  // recoverSet(glob, *gobNoms);
  // glob.Print("V");


  if(importAsimov){
    RooAbsData *asimovdata=wRef->data("asimovData_1");
    w->import(*asimovdata);
  }
  
  w->loadSnapshot("nominalGlobs");
  w->loadSnapshot("nominalNuis");

  w->writeToFile(outputFileName);


}
